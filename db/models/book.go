package models

type Book struct {
	ID          int    `json:"id" gorm:"primary_key;AUTO_INCREMENT"`
	Title       string `json:"title" sql:"index" gorm:"type:varchar(128);unique;not null"`
	Author      string `json:"author"`
	CreatedByID *int   `json:"created_by_id" sql:"index"`
	CreatedBy   *User  `json:"created_by" gorm:"foreignkey:CreatedByID;OnUpdate:DO_NOTHING,OnDelete:SET NULL;"`
	UpdatedByID *int   `json:"updated_by_id" sql:"index"`
	UpdatedBy   *User  `json:"updated_by" gorm:"foreignkey:UpdatedByID;OnUpdate:DO_NOTHING,OnDelete:SET NULL;"`
}
