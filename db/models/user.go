package models

import "time"

type User struct {
	ID        *int      `json:"id" gorm:"primaryKey;autoIncrement:true"`
	Email     string    `json:"email" gorm:"type:varchar(80);unique"`
	Password  string    `json:"password"`
	CreatedAt time.Time `json:"created_at"`
}
