package db

import (
	"log"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"

	"bitbucket.org/hirok_biswas/gocrud/db/models"
)

func Init(connStr string) *gorm.DB {

	db, err := gorm.Open(postgres.Open(connStr), &gorm.Config{})

	if err != nil {
		log.Fatalln(err)
	}
	log.Printf("DB connection established with %s", connStr)

	db.AutoMigrate(&models.User{}, &models.Book{})

	return db
}
