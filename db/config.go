package db

import (
	"fmt"
	"os"
)

func GetDBConnStr() string {
	return fmt.Sprintf(
		"postgres://%s:%s@%s:5432/%s",
		os.Getenv("DB_USER"), os.Getenv("DB_PASS"), os.Getenv("DB_HOST"), os.Getenv("DB_NAME"))
}
