package config

import "os"

var JWT_EXPIRATION = 30
var JWT_SECRET_KEY = os.Getenv("JWT_SECRET_KEY")

var INTERNAL_SERVER_ERROR_MSG = "Something went wrong!"
var DATA_NOT_FOUND_MSG = "Data not found"
