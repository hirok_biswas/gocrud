package adapter

import (
	"net/http"

	"gorm.io/gorm"
)

type App struct {
	DB *gorm.DB
}

type ExHandlerFunc func(db *App, w http.ResponseWriter, r *http.Request)

func With(app *App, xhandler ExHandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		xhandler(app, w, r)
	}
}
