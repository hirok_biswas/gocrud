package handlers

import (
	"github.com/go-chi/chi"
	chiMiddleware "github.com/go-chi/chi/middleware"

	// "github.com/go-chi/cors"
	"bitbucket.org/hirok_biswas/gocrud/adapter"
	auth "bitbucket.org/hirok_biswas/gocrud/handlers/auth"
	book "bitbucket.org/hirok_biswas/gocrud/handlers/book"
)

// GetRouter returns all handler router
func GetRouter(app *adapter.App) *chi.Mux {
	r := chi.NewRouter()

	r.Use(chiMiddleware.Logger)

	// enforce cors policy later
	// r.Use(cors.AllowAll().Handler)

	r.Get("/", adapter.With(app, HealthCheck))

	r.Mount("/auth", auth.Router(app))
	r.Mount("/books", book.Router(app))

	return r
}
