package auth

import jwt "github.com/dgrijalva/jwt-go"

type AccessTokenCustomClaims struct {
	Email   string
	KeyType string
	jwt.StandardClaims
}

type reqLoginPayload struct {
	Email    string `json:"email" validate:"required"`
	Password string `json:"password" validate:"required"`
}

type loginResponse struct {
	AccessToken string `json:"accessToken"`
}
