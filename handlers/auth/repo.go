package auth

import (
	"time"

	"bitbucket.org/hirok_biswas/gocrud/adapter"
	"bitbucket.org/hirok_biswas/gocrud/config"
	"bitbucket.org/hirok_biswas/gocrud/db/models"
	"bitbucket.org/hirok_biswas/gocrud/entity"
	jwt "github.com/dgrijalva/jwt-go"
)

func GetUserByID(app *adapter.App, id *int) (entity.User, error) {
	var user entity.User
	result := app.DB.Where(&entity.User{ID: id}).First(&user)
	return user, result.Error
}

func GetUserByEmail(app *adapter.App, email string) (models.User, error) {
	var user models.User
	result := app.DB.Where(&models.User{Email: email}).First(&user)
	return user, result.Error
}

func GenerateAccessToken(user models.User) (string, error) {
	tokenType := "access"

	// Create the JWT claims, which includes the username and expiry time
	claims := &AccessTokenCustomClaims{
		user.Email,
		tokenType,
		jwt.StandardClaims{
			ExpiresAt: time.Now().Add(time.Minute * time.Duration(config.JWT_EXPIRATION)).Unix(),
		},
	}

	// Declare the token with the algorithm used for signing, and the claims
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	// Create the JWT string
	tokenString, err := token.SignedString([]byte(config.JWT_SECRET_KEY))
	return tokenString, err
}
