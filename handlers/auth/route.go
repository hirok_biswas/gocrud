package auth

import (
	"bitbucket.org/hirok_biswas/gocrud/adapter"
	"github.com/go-chi/chi"
)

func Router(app *adapter.App) *chi.Mux {
	r := chi.NewRouter()

	r.Post("/login", adapter.With(app, Login))

	return r
}
