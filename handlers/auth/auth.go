package auth

import (
	"encoding/json"
	"net/http"

	"bitbucket.org/hirok_biswas/gocrud/adapter"
	"bitbucket.org/hirok_biswas/gocrud/utils"
	"golang.org/x/crypto/bcrypt"
)

func Login(app *adapter.App, w http.ResponseWriter, r *http.Request) {
	// read request body
	var rs reqLoginPayload
	err := json.NewDecoder(r.Body).Decode(&rs)
	if err != nil || (rs.Email == "" || rs.Password == "") {
		errMsg := "email and password must be specified"
		if err != nil {
			errMsg = "Invalid Input"
		}
		utils.ServeJSON(w, http.StatusBadRequest, errMsg, nil, nil)
		return
	}

	user, err := GetUserByEmail(app, rs.Email)
	if err := bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(rs.Password)); err != nil {
		utils.ServeJSON(w, http.StatusUnauthorized, "Authentication Failed!", nil, nil)
		return
	}
	accessToken, err := GenerateAccessToken(user)
	loginResponse := loginResponse{AccessToken: accessToken}

	// hashedPass, err := bcrypt.GenerateFromPassword([]byte(rs.Password), bcrypt.DefaultCost)
	// fmt.Println(string(hashedPass))
	utils.ServeJSON(w, http.StatusCreated, "Login success", loginResponse, nil)
}
