package book

import (
	"bitbucket.org/hirok_biswas/gocrud/adapter"
	"bitbucket.org/hirok_biswas/gocrud/middleware"
	"github.com/go-chi/chi"
)

func Router(app *adapter.App) *chi.Mux {
	r := chi.NewRouter()

	r.Get("/", adapter.With(app, GetAllBooks))
	r.Get("/{id}", adapter.With(app, GetBook))

	r.With(middleware.IsAuthenticated(app)).Post("/", adapter.With(app, AddBook))
	r.With(middleware.IsAuthenticated(app)).Put("/{id}", adapter.With(app, UpdateBook))
	r.With(middleware.IsAuthenticated(app)).Delete("/{id}", adapter.With(app, DeleteBook))

	return r
}
