package book

type reqAddBook struct {
	Title  string `json:"title" validate:"required"`
	Author string `json:"author" validate:"required"`
}

type reqUpdateBook struct {
	Title  *string `json:"title,omitempty"`
	Author *string `json:"author,omitempty"`
}
