package book

import (
	"encoding/json"
	"log"
	"net/http"
	"strconv"

	"github.com/go-chi/chi"

	"bitbucket.org/hirok_biswas/gocrud/adapter"
	"bitbucket.org/hirok_biswas/gocrud/config"
	"bitbucket.org/hirok_biswas/gocrud/db/models"
	"bitbucket.org/hirok_biswas/gocrud/entity"
	"bitbucket.org/hirok_biswas/gocrud/middleware"
	"bitbucket.org/hirok_biswas/gocrud/utils"
)

func GetAllBooks(app *adapter.App, w http.ResponseWriter, r *http.Request) {
	var books []entity.Book
	result := app.DB.Find(&books)
	if result.Error != nil {
		log.Fatalln(result.Error)
	}
	utils.ServeJSON(w, http.StatusOK, "Book list fetched successfully", books, nil)
}

func AddBook(app *adapter.App, w http.ResponseWriter, r *http.Request) {
	loginUser, _ := middleware.GetUserInfoFromCTX(r.Context())

	// read request body
	var rs reqAddBook
	err := json.NewDecoder(r.Body).Decode(&rs)
	if err != nil || (rs.Title == "" || rs.Author == "") {
		errMsg := "title and author must be specified"
		if err != nil {
			errMsg = "Invalid Input"
		}
		utils.ServeJSON(w, http.StatusBadRequest, errMsg, nil, nil)
		return
	}

	book := models.Book{
		Title:       rs.Title,
		Author:      rs.Author,
		CreatedByID: loginUser.ID,
	}

	result := app.DB.Create(&book)
	if result.Error != nil {
		utils.ServeJSON(w, http.StatusInternalServerError, config.INTERNAL_SERVER_ERROR_MSG, nil, nil)
		return
	}
	utils.ServeJSON(w, http.StatusCreated, "Book added successfully", book, nil)
}

func GetBook(app *adapter.App, w http.ResponseWriter, r *http.Request) {
	// get url arg id
	id, _ := strconv.Atoi(chi.URLParam(r, "id"))

	// find book by id
	var book entity.Book
	result := app.DB.First(&book, id)
	if result.Error != nil {
		utils.ServeJSON(w, http.StatusNotFound, config.DATA_NOT_FOUND_MSG, nil, nil)
		return
	}
	utils.ServeJSON(w, http.StatusOK, "Book detail fetched successfully", book, nil)
}

func UpdateBook(app *adapter.App, w http.ResponseWriter, r *http.Request) {
	loginUser, _ := middleware.GetUserInfoFromCTX(r.Context())
	// get url arg id
	id, _ := strconv.Atoi(chi.URLParam(r, "id"))

	var rs reqUpdateBook
	err := json.NewDecoder(r.Body).Decode(&rs)
	if err != nil {
		errMsg := "Invalid Input"
		utils.ServeJSON(w, http.StatusBadRequest, errMsg, nil, nil)
		return
	}

	// find book by id
	var book models.Book
	result := app.DB.First(&book, id)

	if result.Error != nil {
		utils.ServeJSON(w, http.StatusNotFound, config.DATA_NOT_FOUND_MSG, nil, nil)
		return
	}
	updated := false
	if rs.Title != nil {
		book.Title = *rs.Title
		updated = true
	}
	if rs.Author != nil {
		book.Author = *rs.Author
		updated = true
	}
	if updated {
		book.UpdatedByID = loginUser.ID
		app.DB.Save(&book)
	}
	utils.ServeJSON(w, http.StatusOK, "Book updated successfully", book, nil)
}

func DeleteBook(app *adapter.App, w http.ResponseWriter, r *http.Request) {
	// get url arg id
	id, _ := strconv.Atoi(chi.URLParam(r, "id"))

	// find book by id
	var book models.Book
	result := app.DB.First(&book, id)

	if result.Error != nil {
		utils.ServeJSON(w, http.StatusNotFound, config.DATA_NOT_FOUND_MSG, nil, nil)
		return
	}
	app.DB.Delete(&book)
	utils.ServeJSON(w, http.StatusNoContent, "Book Deleted successfully", nil, nil)
}
