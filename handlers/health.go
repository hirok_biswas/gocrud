package handlers

import (
	"net/http"

	"bitbucket.org/hirok_biswas/gocrud/adapter"
	"bitbucket.org/hirok_biswas/gocrud/utils"
)

func HealthCheck(app *adapter.App, w http.ResponseWriter, r *http.Request) {
	utils.ServeJSON(w, http.StatusOK, "GoCrud healthcheck success", nil, nil)
}
