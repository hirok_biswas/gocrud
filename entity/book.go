package entity

type Book struct {
	ID          int    `json:"id"`
	Title       string `json:"title"`
	Author      string `json:"author"`
	CreatedByID *int   `json:"created_by_id"`
	CreatedBy   *User  `json:"created_by""`
	UpdatedByID *int   `json:"updated_by_id"`
	UpdatedBy   *User  `json:"updated_by"`
}
