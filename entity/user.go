package entity

import "time"

type User struct {
	ID        *int      `json:"id"`
	Email     string    `json:"email"`
	CreatedAt time.Time `json:"created_at"`
}
