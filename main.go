package main

import (
	"fmt"
	"log"
	"net/http"
	"os"

	"github.com/joho/godotenv"

	"bitbucket.org/hirok_biswas/gocrud/adapter"
	"bitbucket.org/hirok_biswas/gocrud/db"
	"bitbucket.org/hirok_biswas/gocrud/handlers"
)

func main() {
	err := godotenv.Load(".env")
	if err != nil {
		log.Fatalf("Some error occured. Err: %s \nPlease make sure .env file is present in project directory.", err)
	}
	dbConnStr := db.GetDBConnStr()
	db := db.Init(dbConnStr)

	app := &adapter.App{
		DB: db,
	}

	addr := fmt.Sprintf("%s:%d", "localhost", 8090)
	srv := &http.Server{
		Addr:    addr,
		Handler: handlers.GetRouter(app),
	}
	log.Println("Staring server with address ", addr)
	if err := srv.ListenAndServe(); err != nil {
		log.Fatal("Failed to start http server on :", err)
		os.Exit(-1)
	}
}
