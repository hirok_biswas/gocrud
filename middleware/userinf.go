package middleware

import (
	"context"

	"bitbucket.org/hirok_biswas/gocrud/db/models"
)

// GetUserInfoFromCTX return userinfo from context
func GetUserInfoFromCTX(ctx context.Context) (models.User, bool) {
	usrInf, ok := ctx.Value(userInfoKey).(models.User)
	if !ok {
		return models.User{}, ok
	}
	return usrInf, ok
}
