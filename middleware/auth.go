package middleware

import (
	"context"
	"fmt"
	"net/http"
	"strings"

	"bitbucket.org/hirok_biswas/gocrud/adapter"
	"bitbucket.org/hirok_biswas/gocrud/config"
	auth_repo "bitbucket.org/hirok_biswas/gocrud/handlers/auth"
	"bitbucket.org/hirok_biswas/gocrud/utils"
	jwt "github.com/dgrijalva/jwt-go"
)

const (
	userInfoKey string = "userInfo"
)

func IsAuthenticated(app *adapter.App) func(next http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			authHeader := strings.Split(r.Header.Get("Authorization"), "Bearer ")
			if len(authHeader) != 2 {
				utils.ServeJSON(w, http.StatusUnauthorized, "Missing Auth Token", nil, nil)
				return
			}
			jwtTkn := authHeader[1]
			claims := jwt.MapClaims{}
			_, err := jwt.ParseWithClaims(jwtTkn, claims, func(token *jwt.Token) (interface{}, error) {
				return []byte(config.JWT_SECRET_KEY), nil
			})
			if err != nil {
				utils.ServeJSON(w, http.StatusUnauthorized, err.Error(), nil, nil)
				return
			}
			email := fmt.Sprintf("%v", claims["Email"])
			user, err := auth_repo.GetUserByEmail(app, email)
			if err != nil {
				utils.ServeJSON(w, http.StatusUnauthorized, "User authentication failed", nil, nil)
				return
			}
			// add the user to the context
			ctx := context.WithValue(r.Context(), userInfoKey, user)
			r = r.WithContext(ctx)

			next.ServeHTTP(w, r)
		})
	}
}
