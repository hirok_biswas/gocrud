package utils

import (
	"encoding/json"
	"net/http"
)

type Code string

type Response struct {
	Status  int         `json:"-"`
	Message string      `json:"message,omitempty"`
	Data    interface{} `json:"data,omitempty"`
	Errors  interface{} `json:"errors,omitempty"`
}

func ServeJSON(w http.ResponseWriter, status int, message string, data interface{}, errors interface{}) error {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(status)

	var resp interface{}

	resp = &Response{
		Status:  status,
		Message: message,
		Data:    data,
		Errors:  errors,
	}

	if err := json.NewEncoder(w).Encode(resp); err != nil {
		return err
	}

	return nil
}
